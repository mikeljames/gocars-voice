/* eslint no-shadow:0*/
'use strict';
const frisby = require('frisby');
const expect = require('chai').expect;
const h = require('./helpers');
const HOST = 'http://localhost:3000';

let callId;
let $;

frisby.create('Location is registered')
.post(`${HOST}/tdispatch/call`, {
  'Caller': '+441934440112',
  'Called': '+441934440111'
})
.expectStatus(200)
.after((err, res, body) => {
  $ = h.load(body);
  expect($('Say', 'Response').text()).to.equal('Hi Thanks for calling MikesCabs2We notice your are calling from Voice 1Please enter the number of passengers on your keypadWhoops please, follow the instructionsPlease enter the number of passengers on your keypadGood byeThis call was powered by go caller.co.uk');
})
.toss();


frisby.create('Make a call')
.post(`${HOST}/tdispatch/call`, {
  'Caller': '+441934440113',
  'Called': '+441934440111'
})
.expectStatus(200)
.after((err, res, body) => {
  callId = h.getCallId(body);

  frisby.create('Request cab for 2 passengers')
  .post(`${HOST}/book?callId=${callId}`, {
    'Caller': '+441934440113',
    'Called': '+441934440111',
    'Digits': 2
  })
  .expectStatus(200)
  .after((err, res, body) => {
    $ = h.load(body);
    expect($('Say', 'Response').text()).to.equal('Thanks for booking. You will receive Calls on the status of your Taxi. Thanks for booking with MikesCabs2, Good ByeThis call was powered by go caller.co.uk');

    frisby.create('Make a call')
    .post(`${HOST}/tdispatch/call`, {
      'Caller': '+441934440113',
      'Called': '+441934440111'
    })
    .expectStatus(200)
    .after((err, res, body) => {
      callId = h.getCallId(body);
      frisby.create('Cancel Booking')
      .post(`${HOST}/bookings/list/cancel?callId=${callId}`, {
        'Caller': '+441934440113',
        'Called': '+441934440111',
        'Digits': 1
      })
      .expectStatus(200)
      .after((err, res, body) => {
        $ = h.load(body);
        expect($('Say', 'Response').text()).to.equal('Your booking has been canceled. To make a new booking please continue to listenPlease enter the number of passengers on your keypadWhoops please, follow the instructionsPlease enter the number of passengers on your keypadGood bye');
        callId = h.getCallId(body);
        frisby.create('Request cab for 3 passengers on a cancel call')
        .post(`${HOST}/book?callId=${callId}`, {
          'Caller': '+441934440113',
          'Called': '+441934440111',
          'Digits': 3
        })
        .expectStatus(200)
        .after(() => {
          frisby.create('Get booking status1')
          .post(`${HOST}/tdispatch/call`, {
            'Caller': '+441934440113',
            'Called': '+441934440111'
          })
          .expectStatus(200)
          .after((err, res, body) => {
            $ = h.load(body);
            expect($('Say', 'Response').text()).to.contain('Hi Thanks for calling MikesCabs2We notice your are calling from Voice 2You have 1 booking, to make a new booking press 0. Booking 1 for 3 passengers');
            expect($('Say', 'Response').text()).to.contain('your driver is on the way. to cancel this booking press 1Whoops please, follow the instructionsYou have 1 booking, to make a new booking press 0. Booking 1 for 3 passengers');

            callId = h.getCallId(body);
            frisby.create('Request cab for 4 passengers on a call with bookings')
            .post(`${HOST}/book?callId=${callId}`, {
              'Caller': '+441934440113',
              'Called': '+441934440111',
              'Digits': 4
            })
            .expectStatus(200)
            .after((e, r, body) => {
              $ = h.load(body);
              expect($('Say', 'Response').text()).to.equal('Thanks for booking. You will receive Calls on the status of your Taxi. Thanks for booking with MikesCabs2, Good ByeThis call was powered by go caller.co.uk');

              frisby.create('Get booking status2')
              .post(`${HOST}/tdispatch/call`, {
                'Caller': '+441934440113',
                'Called': '+441934440111'
              })
              .expectStatus(200)
              .after((err, res, body) => {
                $ = h.load(body);
                expect($('Say', 'Response').text()).to.equal('Hi Thanks for calling MikesCabs2We notice your are calling from Voice 2You have 2 bookings, to make a new booking press 0. Booking 1 for 3 passengers your driver is on the way. to cancel this booking press 1,Booking 2 for 4 passengers your driver is on the way. to cancel this booking press 2Whoops please, follow the instructionsYou have 2 bookings, to make a new booking press 0. Booking 1 for 3 passengers your driver is on the way. to cancel this booking press 1,Booking 2 for 4 passengers your driver is on the way. to cancel this booking press 2Good byeThis call was powered by go caller.co.uk');
              }).toss();
            }).toss();
          }).toss();
        }).toss();
      }).toss();
    }).toss();
  }).toss();
}).toss();
