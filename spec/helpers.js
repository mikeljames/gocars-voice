const cheerio = require('cheerio');
const qs = require('qs');

const load = (b) => cheerio.load(b, {normalizeWhitespace: true, xmlMode: true});
const getCallId = b => {
  const query = qs.parse(load(b)('Gather').attr('action'));
  if (!query[Object.keys(query)[0]]) {
    console.log('body with no getCallId', b);
  }
  return query[Object.keys(query)[0]];

};

module.exports = {
  getCallId,
  load
};
