const frisby = require('frisby');
const expect = require('chai').expect;
const h = require('./helpers');

const HOST = 'http://localhost:3000';
frisby.create('Location not registered')
.post(`${HOST}/tdispatch/call`, {
  'Caller': '+441934440110',
  'Called': '+441934440111'
})
.expectStatus(200)
.after((err, res, body) => {
  const $ = h.load(body);
  expect($('Say', 'Response').text()).to.equal('The number you are calling from is not registered. to register this number please visit mikescabs2.com');
})
.toss();
