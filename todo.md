****clear db and create new fleet
ensure password for fleet customer is persisting
test out adding locations with same telephone number
test out oneTimeCode spaceOutNumbers
email fleet emails on signup to confirm email
book a taxi fix errors /
get login working on zoho mail gocaller.co.uk email /
test out callback feature /
validate data in t-dispatch /
build UI and website /


*** TRANSLATIONS
public GET: /languages
[{
  id: 'en-GB',
  "key": "value"
  type: 'language',
},
{
  id: 'es-MX',
  "key": "value",
  type: 'language',
}]

GET: /languages/:id
{
  id: 'en-GB',
  "key": "value"
  type: 'language',
}

GET: /fleets/:id
{
  type: 'fleet',
  language: 'en-GB' // foreign key
  id: <GUID>,
  timezone: <TimeZone>,
}

PUT: languages
with validation for every key
  {
    id: 'en',
    "key": "value"
  }

app.gocaller.co.uk
GET /languages
default to language in browser or something: give option to choose language
maybe add it to the webpack build to have the bundle fully self contained?

all using format.js

- constraints on translations
limited to list of langs on twilio

- changes to api.
on create of fleet send up language store in fleet doc
allow fleet to change language - this by default is the language chosen on app.

- changes to voice.
get fleet returns the translations in one response
