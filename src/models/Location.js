import rp from 'request-promise';
import Url from 'url';
import {BASIC_AUTH_HEADER, BASE_API} from '../config';
console.log(BASIC_AUTH_HEADER);
function findByTelephoneNumber(number) {

  return rp.get({
    uri: Url.resolve(BASE_API, `location/telephone/${number}`),
    headers: {
      'Authorization': BASIC_AUTH_HEADER,
      'Content-Type': 'application/json'
    }
  })
  .then(JSON.parse);
}

module.exports = {
  findByTelephoneNumber
};
