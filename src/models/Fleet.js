import rp from 'request-promise';
import Url from 'url';
import {BASIC_AUTH_HEADER, BASE_API} from '../config';

export function findByTelephoneNumber(called) {
  return rp.get({
    uri: Url.resolve(BASE_API, `fleet/called/${called}`),
    headers: {
      'Authorization': BASIC_AUTH_HEADER,
      'Content-Type': 'application/json'
    }
  })
  .then(JSON.parse);
}
