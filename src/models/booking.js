import rp from 'request-promise';
import {BASIC_AUTH_HEADER, BASE_API} from '../config';

export function book(shortId, booking) {

  const payload = {
    passengerName: booking.passengerName,
    telephoneNumber: booking.telephoneNumber,
    pickUpTime: new Date(),
    passengerCount: booking.passengerCount
  };

  return rp.post({
    url: `${BASE_API}/book/${shortId}`,
    body: JSON.stringify(payload),
    headers: {
      'Authorization': BASIC_AUTH_HEADER,
      'Content-Type': 'application/json'
    }
  })
  .then(JSON.parse);
}

export function cancel({pk, telephoneNumber}) {
  return rp.post({
    url: `${BASE_API}/booking/${pk}/cancel`,
    body: JSON.stringify({
      telephoneNumber,
      description: 'This booking was cancelled via GoCaller.co'
    }),
    headers: {
      'Authorization': BASIC_AUTH_HEADER,
      'Content-Type': 'application/json'
    }
  });
}
