import rp from 'request-promise';
import Url from 'url';
import {BASIC_AUTH_HEADER, BASE_API} from '../config';

export default function(caller, called) {
  return rp.post({
    uri: Url.resolve(BASE_API, `identifycaller`),
    body: JSON.stringify({
      caller,
      called
    }),
    headers: {
      'Authorization': BASIC_AUTH_HEADER,
      'Content-Type': 'application/json'
    }
  })
  .then(JSON.parse);
}
