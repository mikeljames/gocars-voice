import get from 'getenv';
function btoa(str) {
  let buffer;

  if (str instanceof Buffer) {
    buffer = str;
  } else {
    buffer = new Buffer(str.toString(), 'binary');
  }

  return buffer.toString('base64');
}

export const BASE_API = get('BASE_API', 'http://localhost:3001');
console.log(`BASE_API: ${BASE_API}`);
export const isDev = get.string('NODE_ENV', 'production') === 'development';
export const PORT = get('PORT', 3000);
export const CALL_SESSION_TTL = get.int('CALL_SESSION_TTL', 60 * 60);
const VOICE_PASSWORD = get.string('VOICE_PASSWORD', 'test1234');
console.log('voice password', VOICE_PASSWORD);
const basic = btoa(`voice:${VOICE_PASSWORD}`);
export const BASIC_AUTH_HEADER = `Basic ${basic}`;
export const ROLLBAR = get('ROLLBAR', '');
