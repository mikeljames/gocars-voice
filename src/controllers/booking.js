import Twilio from 'twilio';
import session from '../util/session';
import {book, cancel} from '../models/booking';
import messages from '../messages';
import td from 'tdispatch-contrib-booking-types';
import lang from '../util/lang';

export const newBooking = (request, reply) => {
  const callId = request.query.callId;
  const digits = request.payload.Digits;
  const res = new Twilio.TwimlResponse();

  session.get(callId)
  .then(call => {
    const {
      fleet: {
        language,
        twilio: { voice }
      }
    } = call;

    const format = lang(language);

    if (digits === 0) {
      res.say(format('Sorry you entered Zero passengers'), {
        language,
        voice
      });
      messages.newBooking(res, {callId, host: request.info.host, voice, language});
      return reply(res.toString()).header('Content-Type', 'text/xml');
    }

    return book(call.shortId, {
      passengerName: call.name,
      passengerCount: digits,
      telephoneNumber: call.telephoneNumber
    })
    .then(() => {
      res.say(format('Thanks for booking. You will receive Calls on the status of your Taxi. Thanks for booking with {fleetName}, Good Bye', {
        fleetName: call.fleetName
      }),
        {
          language,
          voice
        }
      )
      .pause({
        length: 1
      })
      .say(format('This call was powered by go caller.co.uk'), {
        voice,
        language
      });

      reply(res.toString()).header('Content-Type', 'text/xml');
    });
  })
  .catch((err) => {

    res.say('Sorry we are having some technical difficulties processing this call, please hang up and try again');

    reply(res.toString()).header('Content-Type', 'text/xml');
    console.log(err);
  });

};

export const cancelFromList = (request, reply) => {
  const res = new Twilio.TwimlResponse();
  const digits = parseInt(request.payload.Digits, 10);
  const {callId, pk} = request.query;

  session.get(callId)
  .then((call) => {
    console.log('call.fleet', call.fleet);
    const {
      fleet: {
        language,
        twilio: { voice }
      }
    } = call;
    console.log('fleet.language', language);

    if (digits === 0) {
      messages.newBooking(res, {callId, host: request.info.host, language, voice});
      return reply(res.toString()).header('Content-Type', 'text/xml');
    }

    const bookings = td.filterByCanBeCancelled(call.bookings);

    if (pk) {
      const booking = bookings.find(b => b.pk === pk);
      return cancel({telephoneNumber: booking.passengerPhone, pk});
    }

    const booking = bookings[digits - 1];

    return cancel({telephoneNumber: booking.passengerPhone, pk: booking.pk});
  })
  .then(() => session.get(callId))
  .then((call) => {
    const {
      fleet: {
        language,
        twilio: { voice }
      }
    } = call;

    const format = lang(language);
    res.say(format('Your booking has been canceled. To make a new booking please continue to listen'), {
      voice,
      language
    });

    messages.newBooking(res, {callId, host: request.info.host, voice, language});
    reply(res.toString()).header('Content-Type', 'text/xml');
  })
  .catch(err => {
    console.log(err.stack);
    messages.techIssues(res);
    reply(res.toString()).header('Content-Type', 'text/xml');
  });
};
