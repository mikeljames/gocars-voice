import Boom from 'boom';
import Twilio from 'twilio';
import {types, canBeCancelled} from 'tdispatch-contrib-booking-types';
// import moment from 'moment';
import messages from '../messages';
import * as Fleet from '../models/Fleet';
import Location from '../models/Location';
import session from '../util/session';
import lang from '../util/lang';

module.exports = (request, reply) => {

  const res = new Twilio.TwimlResponse();

  const message = getMessage(request.query.status);
  const passengers = request.query.passengers;
  const pk = request.query.pk;
  const pickupTime = request.query.pickupTime;
  // const hours = pad(moment(pickupTime).get('h'));
  // const minutes = pad(moment(pickupTime).get('m'));
  const status = request.query.status;

  const {From, To} = request.payload;
  console.log('payload', request.payload);
  if (!message) {
    // res.say('Whoops this fleet is not fully setup');
    reply(res.toString()).header('Content-Type', 'text/xml');
  }

  Promise.all([
    Location.findByTelephoneNumber(To),
    Fleet.findByTelephoneNumber(From)
  ])
  .then(([location, fleet]) => {
    const language = fleet.language;
    const voice = fleet.twilio.voice;
    const format = lang(language);

    return session.set({...location, fleetName: fleet.fleetName, fleet})
    .then(callId => {

      if (!fleet) {
        console.log('getting a callback request but fleet does not exist search for fleet number:', From);
        return reply(Boom.badImplementation());
      }

      const booking = location.bookings.find(b => b.pk === pk);
      if (!booking) {
        console.log('error no booking for location pk', pk);
        console.log('HOW DO WE HANDLE THIS WITH TWILIIOOOOOOO!?');
        return reply(Boom.badImplementation());
      }
      // ordered at ${hours} ${minutes} hours
      res
      .say(fleet.voice.callback, {
        voice,
        language
      })
      .say(format('about your booking for num passengers', {
        passengers: booking.passengers
      }), {
        voice,
        language
      })
      .say(format(message), {
        voice,
        language
      });

      if (canBeCancelled(status)) {
        messages.cancellationOptions(res, {host: request.info.host, passengers, voice, language, pickupTime, callId, pk});
      }

      res.say(format('Good bye'), {
        voice,
        language
      });

      reply(res.toString()).header('Content-Type', 'text/xml');
    });
  })
  .catch(e => {
    console.log('error getting location and fleet', e.stack);
    return reply(Boom.badImplementation());
  });
};

function getMessage(status) {
  switch (status) {
    case types.CONFIRMED:
      return 'Your driver has been confirmed';
    case types.ON_THE_WAY:
      return 'Your driver is on the way';
    case types.ARRIVED_WAITING:
      return 'Your driver is outside and waiting';
    case types.CANCELLED:
      return 'Your booking has been cancelled';
    case types.MISSED:
      return 'We\'re sorry we haven\'t been able to allocate you a driver at this time, please try calling again soon.';
    default:
      return null;
  }
}

// function pad(val) {
//   return val > 9 ? val : `0${val}`;
// }
