import Twilio from 'twilio';

module.exports = (request, reply) => {
  const res = new Twilio.TwimlResponse();

  res.say(`Sorry This service is not currently Available via SMS.`);

  reply(res.toString()).header('Content-Type', 'text/xml');

};
