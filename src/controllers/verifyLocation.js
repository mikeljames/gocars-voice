import Twilio from 'twilio';
import * as Fleet from '../models/Fleet';
import Boom from 'boom';
import errorReporting from '../util/errorReporting';
import lang from '../util/lang';

function spaceOutNumbers([a, b, c, d]) {
  return `${a} ${b} ${c} ${d}`;
}

module.exports = (request, reply) => {
  const {oneTimeCode} = request.query;
  const res = new Twilio.TwimlResponse();

  const {From} = request.payload;
  Fleet.findByTelephoneNumber(From)
  .then((fleet) => {
    if (!fleet) {
      console.log(`No fleet returned for payload.From: ${From}`);
      errorReporting.message(`No fleet returned for payload.From: ${From}`);
      return reply(Boom.badImplementation());
    }
    const language = fleet.language;
    const voice = fleet.twilio.voice;
    const format = lang(language);

    res.say(format('Hi this is your activation code for {fleetName}, it is a 4 digit code. If you have not requested this call please ignore', {fleetName: fleet.fleetName}),
      {
        voice,
        language
      }
    );
    const oneTimeCodeSpaced = spaceOutNumbers(oneTimeCode);
    const codeMessage = format('Your code is {oneTimeCodeSpaced} please enter this into the form shown', {oneTimeCodeSpaced});
    res.say(codeMessage,
      {
        voice,
        language
      }
    )
    .pause({
      length: 1
    })
    .say(codeMessage,
      {
        voice,
        language
      }
    )
    .pause({
      length: 1
    })
    .say(codeMessage,
      {
        voice,
        language
      }
    ).say(format('Good bye'));

    reply(res.toString()).header('Content-Type', 'text/xml');
  })
  .catch(e => {
    res.say('An error has occured. Our technical support team has been alerted of this error.');
    console.log(e.stack);
    errorReporting.handleError(e);
    errorReporting.message(`Unable to find fleet by payload.From: ${From}`);
    reply(res).header('Content-Type', 'text/xml');
  });
};
