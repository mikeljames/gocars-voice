import Twilio from 'twilio';
import messages from '../messages';
import session from '../util/session';
import lang from '../util/lang';
import identifyCaller from '../models/identifyCaller';

export default (request, reply) => {

  const res = new Twilio.TwimlResponse();
  const {Caller, Called} = request.payload;

  identifyCaller(Caller, Called)
  .then(({fleet, location, isOneToOne}) => {
    console.log('location', location);
    console.log('fleet', fleet);
    if (!fleet) {
      messages.techIssues(res);
      return reply(res.toString()).header('Content-Type', 'text/xml');
    }
    const language = fleet.language;
    const voice = fleet.twilio.voice;
    const format = lang(language);

    if (!location || !location.name) {
      res.say(fleet.voice.notRegistered, {
        voice,
        language
      });

      return reply(res.toString()).header('Content-Type', 'text/xml');
    }

    session.set({
      ...location,
      fleetName: fleet.fleetName,
      fleet,
      isOneToOne,
      telephoneNumber: Caller // its always caller phone number for normal and isOneToOne
    })
    .then(callId => {
      res.say(fleet.voice.greeting, {
        voice,
        language
      })
      .pause({
        length: 1
      })
      .say(format('We notice your are calling from', {name: location.name}), {
        voice,
        language
      });
      if (location.bookings.length) {
        messages.listBookings(res, {
          host: request.info.host,
          bookings: location.bookings,
          voice,
          language,
          location,
          callId
        });
      } else {
        messages.newBooking(res, {host: request.info.host, language, voice, callId});
      }

      res.say(format('This call was powered by go caller.co.uk'), {
        voice,
        language
      });

      reply(res.toString()).header('Content-Type', 'text/xml');
    })
    .catch(err => {

      messages.techIssues(res, {language, voice});

      console.log('Problem getting data from redis', err);

      reply(res.toString()).header('Content-Type', 'text/xml');
    });
  })
  .catch((err) => {
    console.log(err.stack);
    messages.techIssues(res);
    reply(res.toString()).header('Content-Type', 'text/xml');
  });

};
