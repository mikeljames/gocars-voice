import rollbar from 'rollbar';
import {ROLLBAR} from '../config';
rollbar.init(ROLLBAR);
export default rollbar;
