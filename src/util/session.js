import redis from 'redis';
import uuid from 'uuid';
import { CALL_SESSION_TTL } from '../config';

let client;

if (process.env.NODE_ENV === 'production') {
  client = redis.createClient(process.env.REDIS_URL);
} else {
  client = redis.createClient();
}

function set(callData, ttl) {
  const callId = uuid.v4();

  return new Promise((resolve, reject) => {

    client.set(callId, JSON.stringify(callData), (err) => {
      if (err) return reject(err);

      resolve(callId);

      client.expire(callId, ttl || CALL_SESSION_TTL);
    });

  });
}


function get(key) {
  return new Promise((resolve, reject) => {
    client.get(key, function(err, res) {
      if (err) return reject(err);
      resolve(JSON.parse(res));
    });
  });
}

module.exports = {
  set,
  get
};
