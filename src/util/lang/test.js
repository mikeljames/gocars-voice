import {expect} from 'chai';
import lang from './';

describe('resolve with a message', () => {

  it('we notice you are calling from', () => {
    const format = lang('en-GB');
    const message = format('We notice your are calling from', {name: 'Starbucks Vauxhall Bridge'});
    expect(message).to.equal('We notice your are calling from Starbucks Vauxhall Bridge');
  });

  it('fallback to en on unrecognised lang', () => {
    const format = lang('en-ss');
    const message = format('We notice your are calling from', {name: 'Starbucks Vauxhall Bridge'});
    expect(message).to.equal('We notice your are calling from Starbucks Vauxhall Bridge');
  });

  it('spanish translation for We notice your are calling from', () => {
    const format = lang('es-ES');
    const message = format('We notice your are calling from', {name: 'Starbucks Vauxhall Bridge'});
    expect(message).to.equal('Nos damos cuenta de que su llama desde Starbucks Vauxhall Bridge');
  });

  it('about your booking for {booking.passengers} passenger{pluralize}', () => {
    const format = lang('es-GB');
    const message = format('about your booking for num passengers', {passengers: 5});
    expect(message).to.equal('about your booking for 5 passengers');
  });

  it('about your booking for {booking.passengers} passenger', () => {
    const format = lang('es-GB');
    const message = format('about your booking for num passengers', {passengers: 1});
    expect(message).to.equal('about your booking for 1 passenger');
  });

  it('about your booking for {booking.passengers} passenger', () => {
    const format = lang('es-GB');
    const message = format('about your booking for num passengers', {passengers: 0});
    expect(message).to.equal('about your booking for 0 passengers');
  });

  it('Thanks for booking. You will receive Calls on the status of your Taxi. Thanks for booking with {fleetName}, Good Bye', () => {
    const format = lang('es-GB');
    const message = format('Thanks for booking. You will receive Calls on the status of your Taxi. Thanks for booking with {fleetName}, Good Bye', {fleetName: 'mikes cabs'});
    expect(message).to.equal('Thanks for booking. You will receive Calls on the status of your Taxi. Thanks for booking with mikes cabs, Good Bye');
  });

  it('We\'re sorry we haven\'t been able to allocate you a driver at this time, please try calling again soon.', () => {
    const format = lang('es-GB');
    const message = format('We\'re sorry we haven\'t been able to allocate you a driver at this time, please try calling again soon.');
    expect(message).to.equal('We\'re sorry we haven\'t been able to allocate you a driver at this time, please try calling again soon.');
  });

  it('Booking index for numberOfPassengers passengers status. to cancel this booking press index', () => {
    const format = lang('es-GB');
    const message = format('Booking index for numberOfPassengers passengers status. to cancel this booking press index', {
      index: 1,
      numberOfPassengers: 5,
      status: 'booking is being processed'
    });
    expect(message).to.equal('Booking 1 for 5 passengers booking is being processed. to cancel this booking press 1');
  });

  it('You have count booking, to make a new booking press 0. messagesForEachBooking', () => {
    const format = lang('es-GB');
    const message = format('You have count booking, to make a new booking press 0. messagesForEachBooking', {
      count: 1,
      messagesForEachBooking: 'Booking 1 for 5 passengers booking is being processed. to cancel this booking press 1'
    });
    expect(message).to.equal('You have 1 booking, to make a new booking press 0. Booking 1 for 5 passengers booking is being processed. to cancel this booking press 1');
  });

  it('Booking for {passengers} passenger{s} to cancel this booking press 1', () => {
    const format = lang('es-GB');
    const message = format('Booking for passengersCount passengers to cancel this booking press 1', {
      passengers: 3
    });
    expect(message).to.equal('Booking for 3 passengers to cancel this booking press 1');
  });

});
