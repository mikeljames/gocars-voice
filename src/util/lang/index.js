import IntlMessageFormat from 'intl-messageformat';

if (!global.Intl) {
  require('intl');
}

export const messages = {
  'en': require('./en.json'),
  'en-GB': require('./en.json'),
  'en-AU': require('./en.json'),
  'en-CA': require('./en.json'),
  'en-IN': require('./en.json'),
  'en-US': require('./en.json'),
  'es': require('./es.json'),
  'es-MX': require('./es.json'),
  'es-ES': require('./es.json')
};

const cache = {};

export default lang => (id, optional) => {
  const cacheKey = `${lang}-${id}`;
  const cached = cache[cacheKey];
  const msgs = messages[lang] || messages.en;
  const message = msgs[id];
  if (!message) {
    console.error('NOT FOUND', id);
  }
  const parsed = cached
    ? cached
    : cache[cacheKey] = new IntlMessageFormat(message, lang);

  try {
    return parsed.format(optional);
  } catch (e) {
    console.error('ERROR FORMATTING ', id);
    throw e;
  }
};
