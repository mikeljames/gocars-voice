import Url from 'url';
import td from 'tdispatch-contrib-booking-types';
// import moment from 'moment';
import newBooking from './NewBooking';
import lang from '../util/lang';

export default (res, options) => {
  const host = options.host;
  const callId = options.callId;
  const bookings = options.bookings;
  const voice = options.voice;
  const language = options.language;
  const format = lang(language);
  console.log('bookings', bookings);
  const bookingsThatCanBeCancelled = td.filterByCanBeCancelled(bookings);
  console.log('status', bookings[0].status);
  console.log('bookingsThatCanBeCancelled', bookingsThatCanBeCancelled);
  if (bookingsThatCanBeCancelled.length) {
    const gatherOptions = {
      action: Url.resolve(host, `/bookings/list/cancel?callId=${callId}`),
      numDigits: '1'
    };

    const message = generateMessage(bookingsThatCanBeCancelled, {voice, language});
    return res.gather(gatherOptions, function say() {
      this.say(message, {
        voice,
        language
      });
    })
    .pause({
      length: 1
    })
    .say(format('Whoops please, follow the instructions'), {
      voice,
      language
    })
    .gather(gatherOptions, function say() {
      this.say(message, {
        voice,
        language
      });
    })
    .say(format('Good bye'), {
      voice,
      language
    });
  }

  return newBooking(res, options);
};

function generateMessage(bookingsThatCanBeCancelled, {language}) {
  const length = bookingsThatCanBeCancelled.length;
  const format = lang(language);
  const messagesForEachBooking = bookingsThatCanBeCancelled.map((b, i) => {
    const index = i + 1;
    const numberOfPassengers = b.passengers;
    // const hours = moment(b.pickupTime).get('h');
    // const minutes = moment(b.pickupTime).get('m');
    const status = format(td.readableStatus(b.status));
    return format('Booking index for numberOfPassengers passengers status. to cancel this booking press index', {
      index,
      numberOfPassengers,
      status
    });
  });

  return format('You have count booking, to make a new booking press 0. messagesForEachBooking', {
    count: length,
    messagesForEachBooking
  });
}
