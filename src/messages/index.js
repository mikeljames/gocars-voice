import newBooking from './NewBooking';
import techIssues from './TechIssues';
import listBookings from './ListBookings';
import cancellationOptions from './CancelBooking';
export default {
  newBooking,
  techIssues,
  listBookings,
  cancellationOptions
};
