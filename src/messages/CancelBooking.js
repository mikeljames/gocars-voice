import Url from 'url';
// import moment from 'moment';
import lang from '../util/lang';

export default (res, options) => {
  // TIMEZONE FROM FLEET IN TDISPATCH
  // const pickupTime = options.pickupTime;
  // const hours = moment(pickupTime).get('h');
  // const minutes = moment(pickupTime).get('m');

  const {passengers, host, pk, callId, voice, language} = options;

  const format = lang(language);

  const gatherOptions = {
    action: Url.resolve(host, `/bookings/list/cancel?callId=${callId}&pk=${pk}`),
    numDigits: '1'
  };

  res.gather(gatherOptions, function gather() {
    // ordered at ${hours}  ${minutes} hours,
    // we can't use this as its hard coded to first index in the bookings array.
    this.say(format('Booking for passengersCount passengers to cancel this booking press 1', {passengers}), {
      voice,
      language
    });
  });
};
