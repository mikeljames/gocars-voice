import Url from 'url';
import lang from '../util/lang';

export default (res, options) => {
  const {
    host,
    callId,
    voice,
    language,
    ...rest
  } = options;
  const format = lang(language);
  res.gather({
    action: Url.resolve(host, `/book?callId=${callId}`),
    numDigits: '1'
  }, function say() {
    this.say(format('Please enter the number of passengers on your keypad'), {
      voice,
      language
    });
  })
  .pause({
    length: 1
  })
  .say(format('Whoops please, follow the instructions'), {
    voice,
    language
  })
  .gather({
    action: Url.resolve(host, `/book?callId=${callId}`),
    numDigits: '1'
  }, function say() {
    this.say(format('Please enter the number of passengers on your keypad'), {
      voice,
      language
    });
  })
  .say(format('Good bye'), {
    voice,
    language
  });
};
