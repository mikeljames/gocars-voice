import lang from '../util/lang';

export default (res, {voice = 'man', language = 'en'}) => {
  const format = lang(language);
  res.say(format('Sorry We are having some technical difficulties processing this call. Good Bye'),
    voice,
    language
  );
};
