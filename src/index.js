import Hapi from 'hapi';
import {PORT} from './config';
import answerTheCall from './controllers/answerTheCall';
import locationSms from './controllers/locationSms';
import * as bookingController from './controllers/booking';
import callbackHandler from './controllers/callback';
import verifyLocation from './controllers/verifyLocation';
import Joi from 'joi';
import pkg from '../package.json';

const server = new Hapi.Server();

server.connection({
  port: PORT
});

server.route({
  method: 'POST',
  path: '/tdispatch/call',
  handler: answerTheCall
});

server.route({
  method: 'POST',
  path: '/bookings/list/cancel',
  handler: bookingController.cancelFromList,
  config: {
    validate: {
      query: {
        callId: Joi.string().guid(),
        pk: Joi.string()
      }
    }
  }
});

server.route({
  method: 'POST',
  path: '/book',
  handler: bookingController.newBooking,
  config: {
    validate: {
      query: {
        callId: Joi.string().guid()
      }
    }
  }
});

server.route({
  method: 'POST',
  path: '/status/callback',
  handler: callbackHandler,
  config: {
    validate: {
      query: {
        status: Joi.string().regex(/(completed|incoming|cancelled|missed|on_way_to_job|arrived_waiting)/),
        pickupTime: Joi.date().iso(),
        passengers: Joi.number().min(1).max(6),
        pk: Joi.string().min(24)
      }
    }
  }
});

server.route({
  method: 'POST',
  path: '/location/verify',
  config: {
    validate: {
      query: {
        oneTimeCode: Joi.string().regex(/[0-9]{4}/)
      }
    }
  },
  handler: verifyLocation
});

server.route({
  method: 'POST',
  path: '/sms',
  handler: locationSms
});

server.route({
  method: 'GET',
  path: '/',
  handler: (request, reply) =>
    reply({version: pkg.version})
});

server.start((e) => {
  if (e) {
    console.log(e);
    return process.exit(1);
  }

  console.log(server.info.uri);
});
