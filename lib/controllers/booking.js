'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.cancelFromList = exports.newBooking = undefined;

var _twilio = require('twilio');

var _twilio2 = _interopRequireDefault(_twilio);

var _session = require('../util/session');

var _session2 = _interopRequireDefault(_session);

var _booking2 = require('../models/booking');

var _messages = require('../messages');

var _messages2 = _interopRequireDefault(_messages);

var _tdispatchContribBookingTypes = require('tdispatch-contrib-booking-types');

var _tdispatchContribBookingTypes2 = _interopRequireDefault(_tdispatchContribBookingTypes);

var _lang = require('../util/lang');

var _lang2 = _interopRequireDefault(_lang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var newBooking = exports.newBooking = function newBooking(request, reply) {
  var callId = request.query.callId;
  var digits = request.payload.Digits;
  var res = new _twilio2.default.TwimlResponse();

  _session2.default.get(callId).then(function (call) {
    var _call$fleet = call.fleet;
    var language = _call$fleet.language;
    var voice = _call$fleet.twilio.voice;


    var format = (0, _lang2.default)(language);

    if (digits === 0) {
      res.say(format('Sorry you entered Zero passengers'), {
        language: language,
        voice: voice
      });
      _messages2.default.newBooking(res, { callId: callId, host: request.info.host, voice: voice, language: language });
      return reply(res.toString()).header('Content-Type', 'text/xml');
    }

    return (0, _booking2.book)(call.shortId, {
      passengerName: call.name,
      passengerCount: digits,
      telephoneNumber: call.telephoneNumber
    }).then(function () {
      res.say(format('Thanks for booking. You will receive Calls on the status of your Taxi. Thanks for booking with {fleetName}, Good Bye', {
        fleetName: call.fleetName
      }), {
        language: language,
        voice: voice
      }).pause({
        length: 1
      }).say(format('This call was powered by go caller.co.uk'), {
        voice: voice,
        language: language
      });

      reply(res.toString()).header('Content-Type', 'text/xml');
    });
  }).catch(function (err) {

    res.say('Sorry we are having some technical difficulties processing this call, please hang up and try again');

    reply(res.toString()).header('Content-Type', 'text/xml');
    console.log(err);
  });
};

var cancelFromList = exports.cancelFromList = function cancelFromList(request, reply) {
  var res = new _twilio2.default.TwimlResponse();
  var digits = parseInt(request.payload.Digits, 10);
  var _request$query = request.query;
  var callId = _request$query.callId;
  var pk = _request$query.pk;


  _session2.default.get(callId).then(function (call) {
    console.log('call.fleet', call.fleet);
    var _call$fleet2 = call.fleet;
    var language = _call$fleet2.language;
    var voice = _call$fleet2.twilio.voice;

    console.log('fleet.language', language);

    if (digits === 0) {
      _messages2.default.newBooking(res, { callId: callId, host: request.info.host, language: language, voice: voice });
      return reply(res.toString()).header('Content-Type', 'text/xml');
    }

    var bookings = _tdispatchContribBookingTypes2.default.filterByCanBeCancelled(call.bookings);

    if (pk) {
      var _booking = bookings.find(function (b) {
        return b.pk === pk;
      });
      return (0, _booking2.cancel)({ telephoneNumber: _booking.passengerPhone, pk: pk });
    }

    var booking = bookings[digits - 1];

    return (0, _booking2.cancel)({ telephoneNumber: booking.passengerPhone, pk: booking.pk });
  }).then(function () {
    return _session2.default.get(callId);
  }).then(function (call) {
    var _call$fleet3 = call.fleet;
    var language = _call$fleet3.language;
    var voice = _call$fleet3.twilio.voice;


    var format = (0, _lang2.default)(language);
    res.say(format('Your booking has been canceled. To make a new booking please continue to listen'), {
      voice: voice,
      language: language
    });

    _messages2.default.newBooking(res, { callId: callId, host: request.info.host, voice: voice, language: language });
    reply(res.toString()).header('Content-Type', 'text/xml');
  }).catch(function (err) {
    console.log(err.stack);
    _messages2.default.techIssues(res);
    reply(res.toString()).header('Content-Type', 'text/xml');
  });
};