'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _twilio = require('twilio');

var _twilio2 = _interopRequireDefault(_twilio);

var _messages = require('../messages');

var _messages2 = _interopRequireDefault(_messages);

var _session = require('../util/session');

var _session2 = _interopRequireDefault(_session);

var _lang = require('../util/lang');

var _lang2 = _interopRequireDefault(_lang);

var _identifyCaller = require('../models/identifyCaller');

var _identifyCaller2 = _interopRequireDefault(_identifyCaller);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (request, reply) {

  var res = new _twilio2.default.TwimlResponse();
  var _request$payload = request.payload;
  var Caller = _request$payload.Caller;
  var Called = _request$payload.Called;


  (0, _identifyCaller2.default)(Caller, Called).then(function (_ref) {
    var fleet = _ref.fleet;
    var location = _ref.location;
    var isOneToOne = _ref.isOneToOne;

    console.log('location', location);
    console.log('fleet', fleet);
    if (!fleet) {
      _messages2.default.techIssues(res);
      return reply(res.toString()).header('Content-Type', 'text/xml');
    }
    var language = fleet.language;
    var voice = fleet.twilio.voice;
    var format = (0, _lang2.default)(language);

    if (!location || !location.name) {
      res.say(fleet.voice.notRegistered, {
        voice: voice,
        language: language
      });

      return reply(res.toString()).header('Content-Type', 'text/xml');
    }

    _session2.default.set(_extends({}, location, {
      fleetName: fleet.fleetName,
      fleet: fleet,
      isOneToOne: isOneToOne,
      telephoneNumber: Caller // its always caller phone number for normal and isOneToOne
    })).then(function (callId) {
      res.say(fleet.voice.greeting, {
        voice: voice,
        language: language
      }).pause({
        length: 1
      }).say(format('We notice your are calling from', { name: location.name }), {
        voice: voice,
        language: language
      });
      if (location.bookings.length) {
        _messages2.default.listBookings(res, {
          host: request.info.host,
          bookings: location.bookings,
          voice: voice,
          language: language,
          location: location,
          callId: callId
        });
      } else {
        _messages2.default.newBooking(res, { host: request.info.host, language: language, voice: voice, callId: callId });
      }

      res.say(format('This call was powered by go caller.co.uk'), {
        voice: voice,
        language: language
      });

      reply(res.toString()).header('Content-Type', 'text/xml');
    }).catch(function (err) {

      _messages2.default.techIssues(res, { language: language, voice: voice });

      console.log('Problem getting data from redis', err);

      reply(res.toString()).header('Content-Type', 'text/xml');
    });
  }).catch(function (err) {
    console.log(err.stack);
    _messages2.default.techIssues(res);
    reply(res.toString()).header('Content-Type', 'text/xml');
  });
};