'use strict';

var _twilio = require('twilio');

var _twilio2 = _interopRequireDefault(_twilio);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (request, reply) {
  var res = new _twilio2.default.TwimlResponse();

  res.say('Sorry This service is not currently Available via SMS.');

  reply(res.toString()).header('Content-Type', 'text/xml');
};