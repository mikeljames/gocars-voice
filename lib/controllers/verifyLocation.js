'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _twilio = require('twilio');

var _twilio2 = _interopRequireDefault(_twilio);

var _Fleet = require('../models/Fleet');

var Fleet = _interopRequireWildcard(_Fleet);

var _boom = require('boom');

var _boom2 = _interopRequireDefault(_boom);

var _errorReporting = require('../util/errorReporting');

var _errorReporting2 = _interopRequireDefault(_errorReporting);

var _lang = require('../util/lang');

var _lang2 = _interopRequireDefault(_lang);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function spaceOutNumbers(_ref) {
  var _ref2 = _slicedToArray(_ref, 4);

  var a = _ref2[0];
  var b = _ref2[1];
  var c = _ref2[2];
  var d = _ref2[3];

  return a + ' ' + b + ' ' + c + ' ' + d;
}

module.exports = function (request, reply) {
  var oneTimeCode = request.query.oneTimeCode;

  var res = new _twilio2.default.TwimlResponse();

  var From = request.payload.From;

  Fleet.findByTelephoneNumber(From).then(function (fleet) {
    if (!fleet) {
      console.log('No fleet returned for payload.From: ' + From);
      _errorReporting2.default.message('No fleet returned for payload.From: ' + From);
      return reply(_boom2.default.badImplementation());
    }
    var language = fleet.language;
    var voice = fleet.twilio.voice;
    var format = (0, _lang2.default)(language);

    res.say(format('Hi this is your activation code for {fleetName}, it is a 4 digit code. If you have not requested this call please ignore', { fleetName: fleet.fleetName }), {
      voice: voice,
      language: language
    });
    var oneTimeCodeSpaced = spaceOutNumbers(oneTimeCode);
    var codeMessage = format('Your code is {oneTimeCodeSpaced} please enter this into the form shown', { oneTimeCodeSpaced: oneTimeCodeSpaced });
    res.say(codeMessage, {
      voice: voice,
      language: language
    }).pause({
      length: 1
    }).say(codeMessage, {
      voice: voice,
      language: language
    }).pause({
      length: 1
    }).say(codeMessage, {
      voice: voice,
      language: language
    }).say(format('Good bye'));

    reply(res.toString()).header('Content-Type', 'text/xml');
  }).catch(function (e) {
    res.say('An error has occured. Our technical support team has been alerted of this error.');
    console.log(e.stack);
    _errorReporting2.default.handleError(e);
    _errorReporting2.default.message('Unable to find fleet by payload.From: ' + From);
    reply(res).header('Content-Type', 'text/xml');
  });
};