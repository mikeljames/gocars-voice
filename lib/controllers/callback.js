'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
// import moment from 'moment';


var _boom = require('boom');

var _boom2 = _interopRequireDefault(_boom);

var _twilio = require('twilio');

var _twilio2 = _interopRequireDefault(_twilio);

var _tdispatchContribBookingTypes = require('tdispatch-contrib-booking-types');

var _messages = require('../messages');

var _messages2 = _interopRequireDefault(_messages);

var _Fleet = require('../models/Fleet');

var Fleet = _interopRequireWildcard(_Fleet);

var _Location = require('../models/Location');

var _Location2 = _interopRequireDefault(_Location);

var _session = require('../util/session');

var _session2 = _interopRequireDefault(_session);

var _lang = require('../util/lang');

var _lang2 = _interopRequireDefault(_lang);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (request, reply) {

  var res = new _twilio2.default.TwimlResponse();

  var message = getMessage(request.query.status);
  var passengers = request.query.passengers;
  var pk = request.query.pk;
  var pickupTime = request.query.pickupTime;
  // const hours = pad(moment(pickupTime).get('h'));
  // const minutes = pad(moment(pickupTime).get('m'));
  var status = request.query.status;

  var _request$payload = request.payload;
  var From = _request$payload.From;
  var To = _request$payload.To;

  console.log('payload', request.payload);
  if (!message) {
    // res.say('Whoops this fleet is not fully setup');
    reply(res.toString()).header('Content-Type', 'text/xml');
  }

  Promise.all([_Location2.default.findByTelephoneNumber(To), Fleet.findByTelephoneNumber(From)]).then(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2);

    var location = _ref2[0];
    var fleet = _ref2[1];

    var language = fleet.language;
    var voice = fleet.twilio.voice;
    var format = (0, _lang2.default)(language);

    return _session2.default.set(_extends({}, location, { fleetName: fleet.fleetName, fleet: fleet })).then(function (callId) {

      if (!fleet) {
        console.log('getting a callback request but fleet does not exist search for fleet number:', From);
        return reply(_boom2.default.badImplementation());
      }

      var booking = location.bookings.find(function (b) {
        return b.pk === pk;
      });
      if (!booking) {
        console.log('error no booking for location pk', pk);
        console.log('HOW DO WE HANDLE THIS WITH TWILIIOOOOOOO!?');
        return reply(_boom2.default.badImplementation());
      }
      // ordered at ${hours} ${minutes} hours
      res.say(fleet.voice.callback, {
        voice: voice,
        language: language
      }).say(format('about your booking for num passengers', {
        passengers: booking.passengers
      }), {
        voice: voice,
        language: language
      }).say(format(message), {
        voice: voice,
        language: language
      });

      if ((0, _tdispatchContribBookingTypes.canBeCancelled)(status)) {
        _messages2.default.cancellationOptions(res, { host: request.info.host, passengers: passengers, voice: voice, language: language, pickupTime: pickupTime, callId: callId, pk: pk });
      }

      res.say(format('Good bye'), {
        voice: voice,
        language: language
      });

      reply(res.toString()).header('Content-Type', 'text/xml');
    });
  }).catch(function (e) {
    console.log('error getting location and fleet', e.stack);
    return reply(_boom2.default.badImplementation());
  });
};

function getMessage(status) {
  switch (status) {
    case _tdispatchContribBookingTypes.types.CONFIRMED:
      return 'Your driver has been confirmed';
    case _tdispatchContribBookingTypes.types.ON_THE_WAY:
      return 'Your driver is on the way';
    case _tdispatchContribBookingTypes.types.ARRIVED_WAITING:
      return 'Your driver is outside and waiting';
    case _tdispatchContribBookingTypes.types.CANCELLED:
      return 'Your booking has been cancelled';
    case _tdispatchContribBookingTypes.types.MISSED:
      return 'We\'re sorry we haven\'t been able to allocate you a driver at this time, please try calling again soon.';
    default:
      return null;
  }
}

// function pad(val) {
//   return val > 9 ? val : `0${val}`;
// }