'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.messages = undefined;

var _intlMessageformat = require('intl-messageformat');

var _intlMessageformat2 = _interopRequireDefault(_intlMessageformat);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

if (!global.Intl) {
  require('intl');
}

var messages = exports.messages = {
  'en': require('./en.json'),
  'en-GB': require('./en.json'),
  'en-AU': require('./en.json'),
  'en-CA': require('./en.json'),
  'en-IN': require('./en.json'),
  'en-US': require('./en.json'),
  'es': require('./es.json'),
  'es-MX': require('./es.json'),
  'es-ES': require('./es.json')
};

var cache = {};

exports.default = function (lang) {
  return function (id, optional) {
    var cacheKey = lang + '-' + id;
    var cached = cache[cacheKey];
    var msgs = messages[lang] || messages.en;
    var message = msgs[id];
    if (!message) {
      console.error('NOT FOUND', id);
    }
    var parsed = cached ? cached : cache[cacheKey] = new _intlMessageformat2.default(message, lang);

    try {
      return parsed.format(optional);
    } catch (e) {
      console.error('ERROR FORMATTING ', id);
      throw e;
    }
  };
};