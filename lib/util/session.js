'use strict';

var _redis = require('redis');

var _redis2 = _interopRequireDefault(_redis);

var _uuid = require('uuid');

var _uuid2 = _interopRequireDefault(_uuid);

var _config = require('../config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var client = void 0;

if (process.env.NODE_ENV === 'production') {
  client = _redis2.default.createClient(process.env.REDIS_URL);
} else {
  client = _redis2.default.createClient();
}

function set(callData, ttl) {
  var callId = _uuid2.default.v4();

  return new Promise(function (resolve, reject) {

    client.set(callId, JSON.stringify(callData), function (err) {
      if (err) return reject(err);

      resolve(callId);

      client.expire(callId, ttl || _config.CALL_SESSION_TTL);
    });
  });
}

function get(key) {
  return new Promise(function (resolve, reject) {
    client.get(key, function (err, res) {
      if (err) return reject(err);
      resolve(JSON.parse(res));
    });
  });
}

module.exports = {
  set: set,
  get: get
};