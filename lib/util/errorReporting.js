'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _rollbar = require('rollbar');

var _rollbar2 = _interopRequireDefault(_rollbar);

var _config = require('../config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_rollbar2.default.init(_config.ROLLBAR);
exports.default = _rollbar2.default;