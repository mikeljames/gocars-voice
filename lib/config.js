'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ROLLBAR = exports.BASIC_AUTH_HEADER = exports.CALL_SESSION_TTL = exports.PORT = exports.isDev = exports.BASE_API = undefined;

var _getenv = require('getenv');

var _getenv2 = _interopRequireDefault(_getenv);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function btoa(str) {
  var buffer = void 0;

  if (str instanceof Buffer) {
    buffer = str;
  } else {
    buffer = new Buffer(str.toString(), 'binary');
  }

  return buffer.toString('base64');
}

var BASE_API = exports.BASE_API = (0, _getenv2.default)('BASE_API', 'http://localhost:3001');
console.log('BASE_API: ' + BASE_API);
var isDev = exports.isDev = _getenv2.default.string('NODE_ENV', 'production') === 'development';
var PORT = exports.PORT = (0, _getenv2.default)('PORT', 3000);
var CALL_SESSION_TTL = exports.CALL_SESSION_TTL = _getenv2.default.int('CALL_SESSION_TTL', 60 * 60);
var VOICE_PASSWORD = _getenv2.default.string('VOICE_PASSWORD', 'test1234');
console.log('voice password', VOICE_PASSWORD);
var basic = btoa('voice:' + VOICE_PASSWORD);
var BASIC_AUTH_HEADER = exports.BASIC_AUTH_HEADER = 'Basic ' + basic;
var ROLLBAR = exports.ROLLBAR = (0, _getenv2.default)('ROLLBAR', '');