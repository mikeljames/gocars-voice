'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (caller, called) {
  return _requestPromise2.default.post({
    uri: _url2.default.resolve(_config.BASE_API, 'identifycaller'),
    body: JSON.stringify({
      caller: caller,
      called: called
    }),
    headers: {
      'Authorization': _config.BASIC_AUTH_HEADER,
      'Content-Type': 'application/json'
    }
  }).then(JSON.parse);
};

var _requestPromise = require('request-promise');

var _requestPromise2 = _interopRequireDefault(_requestPromise);

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

var _config = require('../config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }