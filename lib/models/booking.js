'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.book = book;
exports.cancel = cancel;

var _requestPromise = require('request-promise');

var _requestPromise2 = _interopRequireDefault(_requestPromise);

var _config = require('../config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function book(shortId, booking) {

  var payload = {
    passengerName: booking.passengerName,
    telephoneNumber: booking.telephoneNumber,
    pickUpTime: new Date(),
    passengerCount: booking.passengerCount
  };

  return _requestPromise2.default.post({
    url: _config.BASE_API + '/book/' + shortId,
    body: JSON.stringify(payload),
    headers: {
      'Authorization': _config.BASIC_AUTH_HEADER,
      'Content-Type': 'application/json'
    }
  }).then(JSON.parse);
}

function cancel(_ref) {
  var pk = _ref.pk;
  var telephoneNumber = _ref.telephoneNumber;

  return _requestPromise2.default.post({
    url: _config.BASE_API + '/booking/' + pk + '/cancel',
    body: JSON.stringify({
      telephoneNumber: telephoneNumber,
      description: 'This booking was cancelled via GoCaller.co'
    }),
    headers: {
      'Authorization': _config.BASIC_AUTH_HEADER,
      'Content-Type': 'application/json'
    }
  });
}