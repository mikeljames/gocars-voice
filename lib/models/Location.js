'use strict';

var _requestPromise = require('request-promise');

var _requestPromise2 = _interopRequireDefault(_requestPromise);

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

var _config = require('../config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

console.log(_config.BASIC_AUTH_HEADER);
function findByTelephoneNumber(number) {

  return _requestPromise2.default.get({
    uri: _url2.default.resolve(_config.BASE_API, 'location/telephone/' + number),
    headers: {
      'Authorization': _config.BASIC_AUTH_HEADER,
      'Content-Type': 'application/json'
    }
  }).then(JSON.parse);
}

module.exports = {
  findByTelephoneNumber: findByTelephoneNumber
};