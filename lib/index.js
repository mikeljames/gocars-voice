'use strict';

var _hapi = require('hapi');

var _hapi2 = _interopRequireDefault(_hapi);

var _config = require('./config');

var _answerTheCall = require('./controllers/answerTheCall');

var _answerTheCall2 = _interopRequireDefault(_answerTheCall);

var _locationSms = require('./controllers/locationSms');

var _locationSms2 = _interopRequireDefault(_locationSms);

var _booking = require('./controllers/booking');

var bookingController = _interopRequireWildcard(_booking);

var _callback = require('./controllers/callback');

var _callback2 = _interopRequireDefault(_callback);

var _verifyLocation = require('./controllers/verifyLocation');

var _verifyLocation2 = _interopRequireDefault(_verifyLocation);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _package = require('../package.json');

var _package2 = _interopRequireDefault(_package);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var server = new _hapi2.default.Server();

server.connection({
  port: _config.PORT
});

server.route({
  method: 'POST',
  path: '/tdispatch/call',
  handler: _answerTheCall2.default
});

server.route({
  method: 'POST',
  path: '/bookings/list/cancel',
  handler: bookingController.cancelFromList,
  config: {
    validate: {
      query: {
        callId: _joi2.default.string().guid(),
        pk: _joi2.default.string()
      }
    }
  }
});

server.route({
  method: 'POST',
  path: '/book',
  handler: bookingController.newBooking,
  config: {
    validate: {
      query: {
        callId: _joi2.default.string().guid()
      }
    }
  }
});

server.route({
  method: 'POST',
  path: '/status/callback',
  handler: _callback2.default,
  config: {
    validate: {
      query: {
        status: _joi2.default.string().regex(/(completed|incoming|cancelled|missed|on_way_to_job|arrived_waiting)/),
        pickupTime: _joi2.default.date().iso(),
        passengers: _joi2.default.number().min(1).max(6),
        pk: _joi2.default.string().min(24)
      }
    }
  }
});

server.route({
  method: 'POST',
  path: '/location/verify',
  config: {
    validate: {
      query: {
        oneTimeCode: _joi2.default.string().regex(/[0-9]{4}/)
      }
    }
  },
  handler: _verifyLocation2.default
});

server.route({
  method: 'POST',
  path: '/sms',
  handler: _locationSms2.default
});

server.route({
  method: 'GET',
  path: '/',
  handler: function handler(request, reply) {
    return reply({ version: _package2.default.version });
  }
});

server.start(function (e) {
  if (e) {
    console.log(e);
    return process.exit(1);
  }

  console.log(server.info.uri);
});