'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _lang = require('../util/lang');

var _lang2 = _interopRequireDefault(_lang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (res, _ref) {
  var _ref$voice = _ref.voice;
  var voice = _ref$voice === undefined ? 'man' : _ref$voice;
  var _ref$language = _ref.language;
  var language = _ref$language === undefined ? 'en' : _ref$language;

  var format = (0, _lang2.default)(language);
  res.say(format('Sorry We are having some technical difficulties processing this call. Good Bye'), voice, language);
};