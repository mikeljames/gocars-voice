'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

var _lang = require('../util/lang');

var _lang2 = _interopRequireDefault(_lang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (res, options) {
  // TIMEZONE FROM FLEET IN TDISPATCH
  // const pickupTime = options.pickupTime;
  // const hours = moment(pickupTime).get('h');
  // const minutes = moment(pickupTime).get('m');

  var passengers = options.passengers;
  var host = options.host;
  var pk = options.pk;
  var callId = options.callId;
  var voice = options.voice;
  var language = options.language;


  var format = (0, _lang2.default)(language);

  var gatherOptions = {
    action: _url2.default.resolve(host, '/bookings/list/cancel?callId=' + callId + '&pk=' + pk),
    numDigits: '1'
  };

  res.gather(gatherOptions, function gather() {
    // ordered at ${hours}  ${minutes} hours,
    // we can't use this as its hard coded to first index in the bookings array.
    this.say(format('Booking for passengersCount passengers to cancel this booking press 1', { passengers: passengers }), {
      voice: voice,
      language: language
    });
  });
};
// import moment from 'moment';