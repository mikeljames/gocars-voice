'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
// import moment from 'moment';


var _url = require('url');

var _url2 = _interopRequireDefault(_url);

var _tdispatchContribBookingTypes = require('tdispatch-contrib-booking-types');

var _tdispatchContribBookingTypes2 = _interopRequireDefault(_tdispatchContribBookingTypes);

var _NewBooking = require('./NewBooking');

var _NewBooking2 = _interopRequireDefault(_NewBooking);

var _lang = require('../util/lang');

var _lang2 = _interopRequireDefault(_lang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (res, options) {
  var host = options.host;
  var callId = options.callId;
  var bookings = options.bookings;
  var voice = options.voice;
  var language = options.language;
  var format = (0, _lang2.default)(language);
  console.log('bookings', bookings);
  var bookingsThatCanBeCancelled = _tdispatchContribBookingTypes2.default.filterByCanBeCancelled(bookings);
  console.log('status', bookings[0].status);
  console.log('bookingsThatCanBeCancelled', bookingsThatCanBeCancelled);
  if (bookingsThatCanBeCancelled.length) {
    var _ret = function () {
      var gatherOptions = {
        action: _url2.default.resolve(host, '/bookings/list/cancel?callId=' + callId),
        numDigits: '1'
      };

      var message = generateMessage(bookingsThatCanBeCancelled, { voice: voice, language: language });
      return {
        v: res.gather(gatherOptions, function say() {
          this.say(message, {
            voice: voice,
            language: language
          });
        }).pause({
          length: 1
        }).say(format('Whoops please, follow the instructions'), {
          voice: voice,
          language: language
        }).gather(gatherOptions, function say() {
          this.say(message, {
            voice: voice,
            language: language
          });
        }).say(format('Good bye'), {
          voice: voice,
          language: language
        })
      };
    }();

    if ((typeof _ret === 'undefined' ? 'undefined' : _typeof(_ret)) === "object") return _ret.v;
  }

  return (0, _NewBooking2.default)(res, options);
};

function generateMessage(bookingsThatCanBeCancelled, _ref) {
  var language = _ref.language;

  var length = bookingsThatCanBeCancelled.length;
  var format = (0, _lang2.default)(language);
  var messagesForEachBooking = bookingsThatCanBeCancelled.map(function (b, i) {
    var index = i + 1;
    var numberOfPassengers = b.passengers;
    // const hours = moment(b.pickupTime).get('h');
    // const minutes = moment(b.pickupTime).get('m');
    var status = format(_tdispatchContribBookingTypes2.default.readableStatus(b.status));
    return format('Booking index for numberOfPassengers passengers status. to cancel this booking press index', {
      index: index,
      numberOfPassengers: numberOfPassengers,
      status: status
    });
  });

  return format('You have count booking, to make a new booking press 0. messagesForEachBooking', {
    count: length,
    messagesForEachBooking: messagesForEachBooking
  });
}