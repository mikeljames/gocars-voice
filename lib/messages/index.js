'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _NewBooking = require('./NewBooking');

var _NewBooking2 = _interopRequireDefault(_NewBooking);

var _TechIssues = require('./TechIssues');

var _TechIssues2 = _interopRequireDefault(_TechIssues);

var _ListBookings = require('./ListBookings');

var _ListBookings2 = _interopRequireDefault(_ListBookings);

var _CancelBooking = require('./CancelBooking');

var _CancelBooking2 = _interopRequireDefault(_CancelBooking);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  newBooking: _NewBooking2.default,
  techIssues: _TechIssues2.default,
  listBookings: _ListBookings2.default,
  cancellationOptions: _CancelBooking2.default
};