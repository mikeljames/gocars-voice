'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

var _lang = require('../util/lang');

var _lang2 = _interopRequireDefault(_lang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

exports.default = function (res, options) {
  var host = options.host;
  var callId = options.callId;
  var voice = options.voice;
  var language = options.language;

  var rest = _objectWithoutProperties(options, ['host', 'callId', 'voice', 'language']);

  var format = (0, _lang2.default)(language);
  res.gather({
    action: _url2.default.resolve(host, '/book?callId=' + callId),
    numDigits: '1'
  }, function say() {
    this.say(format('Please enter the number of passengers on your keypad'), {
      voice: voice,
      language: language
    });
  }).pause({
    length: 1
  }).say(format('Whoops please, follow the instructions'), {
    voice: voice,
    language: language
  }).gather({
    action: _url2.default.resolve(host, '/book?callId=' + callId),
    numDigits: '1'
  }, function say() {
    this.say(format('Please enter the number of passengers on your keypad'), {
      voice: voice,
      language: language
    });
  }).say(format('Good bye'), {
    voice: voice,
    language: language
  });
};